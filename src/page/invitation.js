import React, { useReducer } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import AudioPlayer from "react-audio-player";

const initialState = {
  isPlaying: false,
};

const reducer = (state, action) => {
  switch (action.type) {
    case "PLAY":
      return { ...state, isPlaying: true };
    case "PAUSE":
      return { ...state, isPlaying: false };
    default:
      return state;
  }
};

const Opening = () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const handlePlayAudio = () => {
    dispatch({ type: "PLAY" });
  };

  const backgroundStyle = {
    backgroundImage: `url(/profil.png)`,
    backgroundSize: "cover",
    backgroundPosition: "center",
    position: "absolute",
    top: 0,
    left: 0,
    width: "100vw",
    height: "100vh",
    opacity: '0.5',
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    color: "white",
    textAlign: "center",
    padding: "10px",
  };

  const flexContainer = {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: "20px",
    padding: "20px",
    backgroundColor: "rgba(0, 0, 0, 0.3)",
    borderRadius: "15px",
    border: "2px solid white",
    width: "80%", // Mengatur lebar agar responsif di tampilan mobile
    maxWidth: "400px", // Lebar maksimum agar tidak terlalu lebar di layar besar
  };

  return (
    <div style={backgroundStyle}>
      <div className="absolute top-0 left-0 mt-4 mr-4 overflow-hidden">
        <AudioPlayer
          src="/music/audio.mp3"
          autoPlay={false}
          controls={false}
          playing={state.isPlaying}
        />
        <img
          src="/flower/flower 2.png"
          alt="Gambar 1"
          className="w-40 h-40 transform rotate-180"
          style={{
            objectPosition: "100%",
            objectFit: "cover",
            transform: "translate(-50%)",
          }}
        />
      </div>

      <h1 className="text-2xl md:text-4xl font-bold absolute top-32">
        The Wedding of
      </h1>
      <h1 className="text-4xl md:text-6xl absolute top-48 right-72 left-20">
        Mustika
      </h1>
      <h1 className="text-4xl md:text-6xl absolute top-60 right-72 left-40">
        &
      </h1>
      <h1 className="text-4xl md:text-6xl absolute top-72 right-72 left-52">
        Wira
      </h1>
      <p className="text-sm md:text-base text-pink-500">
        Kepada Yth. Bapak/Ibu/Saudara/i
      </p>

      <div style={flexContainer}>
        <p className="text-sm md:text-base">
          Arya dan Sekeluarga, Kami dengan hormat mengundang anda agar dapat
          hadir di acara pernikahan kami.
        </p>
      </div>
      <Link
        to="/first"
        className="btn btn-primary mt-2 flex items-center justify-center"
        onClick={handlePlayAudio}
      >
        <FontAwesomeIcon icon={faEnvelope} />
        <span className="ml-2">Buka Undangan</span>
      </Link>
    </div>
  );
};

export default Opening;