import React, { useState } from "react";
import AudioPlayer from "react-audio-player";
import Snowfall from "react-snowfall";

const createImage = (src) => {
  const image = document.createElement("img");
  image.src = src;
  return image;
};

const FirstPage = () => {
  const [isPlaying, setIsPlaying] = useState(false);

  const togglePlay = () => {
    setIsPlaying(!isPlaying);
  };
  const backgroundStyle = {
    backgroundImage: `url(/background/bg-1.jpg)`,
    backgroundSize: "cover",
    backgroundPosition: "center",
    position: "relative",
    top: 0,
    left: 0,
    width: "100vw",
    height: "100vh",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    color: "white",
    textAlign: "center",
    padding: "",
  };

  const backgroundStyle2 = {
    ...backgroundStyle,
    backgroundImage: `url(/background/bg-2.jpg)`,
  };

  const flexContainer = {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    borderRadius: "15px",
    width: "80%",
    maxWidth: "400px",
  };

  const leaf1 = createImage("/leaf/Leaf 1.png");
  const leaf2 = createImage("/leaf/Leaf 2.png");

  const images = [leaf1, leaf2];

  const mobileMediaQuery = `@media (max-width: 768px)`;

  const mobileBackgroundStyle = {
    ...backgroundStyle,
    [mobileMediaQuery]: {
      backgroundSize: "contain",
      padding: "20px",
    },
  };

  const mobileBackgroundStyle2 = {
    ...backgroundStyle2,
    [mobileMediaQuery]: {
      backgroundSize: "contain",
      padding: "20px",
    },
  };

  return (
    <div className="absolute overflow-hidden">
      <AudioPlayer
        src="/music/audio.mp3"
        autoPlay
        controls
        volume={0.2}
        style={{
          width: "0",
          height: "0",
          overflow: "hidden",
          position: "absolute",
          zIndex: "-1",
          opacity: "0",
        }}
      />
      <div style={mobileBackgroundStyle}>
        <Snowfall snowflakeCount={500} images={images} />
        {/* start page atas */}
        <img
          src="/flower/flower 2.png"
          alt="Gambar 1"
          className="w-40 h-40 transform rotate-180 absolute top-24 left-72 opacity-80"
          style={{
            objectPosition: "100%",
            objectFit: "cover",
            transform: "scaleX(-1)",
          }}
        />
        <div style={flexContainer}>
          <img
            src="/profil.png"
            alt="Gambar 1"
            className="rounded-full absolute top-28 w-56 h-96 right-24"
          />
        </div>
        <h1 className="text-4xl text-gray-600 font-serif absolute bottom-72 left-10">
          The Wedding of
        </h1>
        <h1
          className="text-6xl text-gray-600 font-mono absolute bottom-40 left-28"
          style={{ fontFamily: "Madina" }}
        >
          Mustika & <br /> Wira
        </h1>
        <h1
          className="text-3xl text-gray-600 font-mono absolute bottom-32 left-28"
          style={{ fontFamily: "Amperzand" }}
        >
          25 | 05 | 22
        </h1>
        {/* end page atas */}
      </div>
      <div style={mobileBackgroundStyle2}>
        {/* start page bawah */}
        <Snowfall snowflakeCount={500} images={images} />

        <img
          src="/flower/flower 1.png"
          alt="Gambar 1"
          className="w-40 h-40 absolute opacity-80 top-0 left-0 right-20"
          style={{
            objectPosition: "100%",
            objectFit: "cover",
            transform: "scaleY(-1)",
          }}
        />
        <article class="absolute top-28">
          <h1 className="text-2xl text-orange-700" style={{ fontFamily: "CursiveSans" }}>
            Assalamualaikum Wr. Wb.
          </h1>
          <p className="text-base text-neutral-600" style={{fontFamily: 'Robecha'}}>Tanpa Mengurangi Rasa Hormat,</p>
          <p className="text-base text-neutral-600" style={{fontFamily: 'Robecha'}}>Kami Mengundang Bapak/Ibu/Saudara/i</p>
          <p className="text-base text-neutral-600" style={{fontFamily: 'Robecha'}}>Di Acara Pernikahan Kami :</p>
          <br></br>
          <h1 className="text-6xl text-gray-700" style={{fontFamily: 'Alegreya'}}>Mustika</h1>
          <p className="text-base text-gray-700" style={{fontFamily: 'Alegreya'}}>Putri dari Bpk Winarto & Ibu Julaikah</p>
          <h1 className="text-6xl text-gray-700" style={{fontFamily: 'Alegreya'}}>&</h1>
          <h1 className="text-6xl text-gray-700" style={{fontFamily: 'Alegreya'}}>Wira</h1>
          <p className="text-base text-gray-700" style={{fontFamily: 'Alegreya'}}>Putri dari Bpk Winarto & Ibu Julaikah</p>
          <br></br>
          <h1 className="text-4xl text-orange-700 italic">Q.S Ar-Rum(30:21)</h1>
          <p
            className="text-xs text-neutral-600"
            style={{ fontFamily: "Amperzand" }}
          >
            Dan di antara tanda-tanda (kebesaran)-Nya ialah
            <br />
            Dia menciptakan pasangan-pasangan untukmu <br />
            dari jenismu sendiri, agar <br />
            kamu cenderung dan merasa tenteram kepadanya, <br />
            dan Dia menjadikan diantaramu rasa kasih dan sayang. <br />
            Sungguh, pada yang demikian itu benar-benar terdapat <br />
            tanda-tanda (kebesaran Allah) bagi kaum yang berpikir.
          </p>
        </article>
        {/* end page bawah */}
      </div>
    </div>
  );
};

export default FirstPage;
