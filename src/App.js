import 'daisyui/dist/full.css';
import './App.css';
import Opening from './page/invitation';
import { GlobalProvider } from './context/GlobalContext';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import FirstPage from './page/letter';


function App() {
  return (
    <div className="App">

      <BrowserRouter>
      <GlobalProvider>
        <Routes>
        <Route path="/" element={<Opening></Opening>}/>
        <Route path="/first" element={<FirstPage/>} />
        </Routes>
      </GlobalProvider>
      </BrowserRouter>
      
      {/* <Logo /> */}
    </div>
  );
}

export default App;
